close all;
clear all;
clc;

No_stations=20;
No_slots=10000; 
packet_arrival=[1:20 25:5:50 60:10:200]; 

G=zeros(1,length(packet_arrival));
S_analyt=zeros(1,length(packet_arrival));
S_sim=zeros(1,length(packet_arrival));
packets = zeros(1,length(packet_arrival));
collisions = zeros(1,length(packet_arrival));

for lambda_set=1:length(packet_arrival) %hlavní loop
    lambda=packet_arrival(1, lambda_set);
    %nulování
    packets_pass = 0;
    packets_coll = 0;
    for slot=1:No_slots %loop přes všechny sloty
        StationsTransmitting = 0;
        if slot == 1 %% vygenerování času do vysílání
            StationTimeToTx=poissrnd(lambda,1,No_stations);% čas do vysilání se řídí poissonovým rozdělením
        end
        for s = 1:No_stations %pokud čas do vysílání je větší jak 0 tak dekrementuju
            if StationTimeToTx(s) > 0
                StationTimeToTx(s) = StationTimeToTx(s) - 1;
            else % jinak inkrementuju a přegeneruju čas do vysílání
                StationsTransmitting = StationsTransmitting + 1;
                StationTimeToTx(s)=poissrnd(lambda);

            end
        end
        if(StationsTransmitting == 1) % pokud vysílá jedna stanice -> super packet prošel
            packets_pass = packets_pass + 1;
        elseif(StationsTransmitting > 1) % pokud vysílá více stanic -> kolize
            packets_coll = packets_coll + StationsTransmitting;
        end
    end
    packets(lambda_set)=packets_pass; %packet    
    collisions(lambda_set)=packets_coll;
    G(lambda_set)=(packets_pass + packets_coll)/No_slots;   
    fprintf("Lambda = %d/%d\n", lambda_set, length(packet_arrival))
end

S_sim = packets./No_slots;
S_analyt = G.*exp(-G);

LS=12;
h1=figure(1);
subplot(211);
plot(G,S_analyt,'b');
sgtitle(sprintf('ALOHA :)'));
hold on
plot(G,S_sim,'r');
legend('Slotted ALOHA: analyt','Slotted ALOHA: simulated','Location','NorthEast');
xlabel('G');
ylabel('S');
set(gca,'FontWeight','normal',...
    'FontSize',LS);%12
grid on;
print('-f1', '-r300', '-dpng','Results')
subplot(212);
plot(G,packets./(collisions+packets),'b');
hold on
plot(G,collisions./(collisions+packets),'r');
legend('Sucessful packets','Collisions','Location','NorthEast');
xlabel('G');
ylabel('Probability');
set(gca,'FontWeight','normal',...
    'FontSize',LS);%12
grid on;

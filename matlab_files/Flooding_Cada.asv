close all
clear;
clc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%1. Step: Define basic parameters%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Simulation_area = [500 500];  % Simulation area in m 
No_nodes = 500; % Definition of number of nodes
Sim_drops = 100;  % Definition of number of drops (for outer loop)
Max_comm_d = 10:5:100; % Definition of max. comm. distance in m (for inner loop) 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%2. Step: Define variables to save results%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Total_messages=zeros(19,3);
Total_hops=zeros(19,3);

% START OF OUTER LOOP (For averaging over simmulation drops)
%---------------------------------------------------
%---------------------------------------------------

for K=1:Sim_drops
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %3. Step: Randomly generate positions of each node%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    Nodes_position=zeros(No_nodes,2);
    for node=1:No_nodes
        Nodes_position(node,1)=rand*Simulation_area(1,1); % x coordinates
        Nodes_position(node,2)=rand*Simulation_area(1,2); % y coordinates
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %4. Step: Select randomly one source (S) and one destination (D)%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    Index_S_D=zeros(2,1); % parameter to store indexes of S and D
    Index_S_D(1,1)=ceil(rand*No_nodes); % Index of S node
    D=0;
    while D==0
        Index_S_D(2,1)=ceil(rand*No_nodes); % Index of D node
        if Index_S_D(2,1)~=Index_S_D(1,1) % Check if S and D are different   
            D=1;
        end
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %5. Step: Calculation of distance between nodes%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    Node_distance=zeros(No_nodes,No_nodes);
    for i=1:No_nodes
        for j=1:No_nodes
            Node_distance(i,j)=sqrt((abs(Nodes_position(i,1)-Nodes_position(j,1)))^2 + (abs(Nodes_position(i,2)-Nodes_position(j,2)))^2);
        end
    end
    
    
    % START OF INNER LOOP (Change of communication range)
    %---------------------------------------------------               
     
    for CommRange=1:19
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %6. Step: Set communication range%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %% Flooding
        Max_comm_distance = Max_comm_d(1,CommRange); % Calculation of current comm range
        device_role = zeros(No_nodes, 1); % nody
        device_role(Index_S_D(1,1)) = 1; % source    
        Time_to_live = 100;
        % nulovani
        messages = 0;
        device_role_curr = zeros(No_nodes,1);
        while Time_to_live > 0
            for i = 1:No_nodes
                if(device_role(i) == 1) %pokud je zařízení source
                    messages = messages + 1;
                    device_role(i) = 0;
                    for j = 1:No_nodes
                        if ((Node_distance(i,j) <= Max_comm_distance)&& (i ~= j))
                            device_role_curr(j) = 1;
                        end
                    end
                end
            end
            Time_to_live = Time_to_live - 1; %dekrementace
            device_role = device_role_curr;
            if  device_role(Index_S_D(2,1)) == 1 
                break
            end
        end
        Total_messages(CommRange, 1) = Total_messages(CommRange,1) + messages;
        Total_hops(CommRange,1) = Total_hops(CommRange,1) + (100 - Time_to_live);

        %% Improved Flooding
        device_role = zeros(No_nodes, 1);%nody
        device_role(Index_S_D(1,1)) = 1;%sourcy      
        Time_to_live = 100;    
        messages = 0;     
        device_role_curr = zeros(No_nodes, 1);
        while(Time_to_live > 0)
            for i = 1:No_nodes
                if(device_role(i) == 1)
                    messages = messages + 1;
                    device_role(i) = 0;
                    device_role_curr(i) = 2;
                    for j = 1:No_nodes
                        if ((Node_distance(i,j) <= Max_comm_distance) && (i ~= j) && device_role_curr(j) ~= 2)
                                device_role_curr(j) = 1;                      
                        end                            
                    end                    
                end                
            end 
            Time_to_live = Time_to_live-1;
            device_role = device_role_curr; 
            if  device_role(Index_S_D(2,1)) == 1               
                break;            
            end   
        end  
        Total_messages(CommRange,2) = Total_messages(CommRange,2) + messages;
        Total_hops(CommRange,2) = Total_hops(CommRange,2) + (100 - Time_to_live); 
        %% Gossiping
        device_role = zeros(No_nodes, 1);%nodes
        device_role(Index_S_D(1,1)) = 1;%source      
        Time_to_live = 100;    
        messages = 0;     
        device_role_curr = zeros(No_nodes, 1);
        p = 0.5;        
        if(rand >= p)  
            device_role(Index_S_D(1,1)) = 1;
        end
        while(Time_to_live > 0)
            for i = 1:No_nodes
                if(device_role(i) == 1)
                    messages = messages + 1;
                    device_role(i) = 0;
                    device_role_curr(i) = 2;
                    for j = 1:No_nodes
                        if ((Node_distance(i,j) <= Max_comm_distance) && (i ~= j) && (device_role_curr(j) ~= 2) && (rand >= p))                        
                            device_role_curr(j) = 1;                                                   
                        end                            
                    end                    
                end                
            end 
            Time_to_live = Time_to_live-1;            
            device_role = device_role_curr;     
            if  device_role(Index_S_D(2,1)) == 1               
                break;            
            end   
        end  
        Total_messages(CommRange,3) = Total_messages(CommRange,3) + messages;
        Total_hops(CommRange,3) = Total_hops(CommRange,3) + (100 - Time_to_live); 
    end
    
    fprintf("K = %d\n",K);   
end
    
    % END of INNER LOOP
    %---------------------------------------------------
    
    

% END OF OUTER LOOP
%---------------------------------------------------
%---------------------------------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%8. Step: Average the results over K simulation drops%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Total_messages=Total_messages/Sim_drops;
Total_hops=Total_hops/Sim_drops;





%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%9. Step: Plot the results%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%


figure (1)
hold on
grid on
axis([1 19 0 ceil(max(Total_messages(:,1)))]);

set(gca, 'fontweight', 'normal', ...
    'fontsize', 14)
xlabel('Maximum communication range [m]')
ylabel('Average number of messages')
a=plot(1:19,Total_messages(1:19,1),'b>-','MarkerSize',6,'LineWidth',1);
b=plot(1:19,Total_messages(1:19,2),'r*-','MarkerSize',6,'LineWidth',1);
c=plot(1:19,Total_messages(1:19,3),'g+-','MarkerSize',6,'LineWidth',1);
set(gca,'XTick',[1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19])
set(gca,'XTickLabel',{'10','15','20','25','30','35','40','45','50','55','60','65','70','75','80','85','90','95','100'})
legend([a,b,c],'Basic flooding','Improved flooding','Gossiping (p=0.5)','Location','northeast');
print('-f1', '-r300', '-dpng','Messages')

figure (2)
hold on
grid on
axis([1 19 0 ceil(max(Total_hops(:,1)))]);

set(gca, 'fontweight', 'normal', ...
    'fontsize', 14)
xlabel('Maximum communication range [m]')
ylabel('Average number of hops')
a=plot(1:19,Total_hops(1:19,1),'b>-','MarkerSize',6,'LineWidth',1);
b=plot(1:19,Total_hops(1:19,2),'r*-','MarkerSize',6,'LineWidth',1);
c=plot(1:19,Total_hops(1:19,3),'g+-','MarkerSize',6,'LineWidth',1);
set(gca,'XTick',[1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19])
set(gca,'XTickLabel',{'10','15','20','25','30','35','40','45','50','55','60','65','70','75','80','85','90','95','100'})
legend([a,b,c],'Basic flooding','Improved flooding','Gossiping (p=0.5)','Location','northeast');
print('-f2', '-r300', '-dpng','Hops')


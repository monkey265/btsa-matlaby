close all
clear all

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%1. Step: Define number of stations and number of slots in the simulation%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
No_stations = 20;
No_slots = 10000; 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%2. Step: Predefine packet arrival (lambda)%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
packet_arrival = [1:20 25:5:50 60:10:200];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%3. Step: Define variables to save results%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
G = zeros(1, length(packet_arrival));
S_analyt = zeros(1, length(packet_arrival));
S_sim = zeros(1, length(packet_arrival));
packets = zeros(1, length(packet_arrival));
collisions = zeros(1, length(packet_arrival));

% START OF OUTER LOOP (For different values of lambda)
% ---------------------------------------------------
% ---------------------------------------------------

for lambda_set = 1:length(packet_arrival)
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %4. Step: Select current lambda value used for Poisson%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    lambda = packet_arrival(lambda_set);

    packets_ok = 0;
    packets_col = 0;

    % START OF INNER LOOP (over all slots)
    % ---------------------------------------------------
    
    for slot = 1:No_slots
        
        % Generate initial packet arrival for each station (only for the first slot)
        if slot == 1
            StationTimeToTx = poissrnd(lambda, 1, No_stations);
        else
            % Update StationTimeToTx for each station
            for station = 1:No_stations
                if StationTimeToTx(station) > 0
                    StationTimeToTx(station) = StationTimeToTx(station) - 1;
                    % Check if the station is ready to transmit
                    if StationTimeToTx(station) == 0
                        fprintf('Station %d is ready to transmit in slot %d\n', station, slot);
                        StationTimeToTx(station) = poissrnd(lambda); % generating packet
                        packets_ok = packets_ok + 1;
                    elseif StationTimeToTx(station) > 1
                        packets_col = packets_col + 1;
                    end
                end
            end
        end
        
        % Find transmitting stations for the current slot
        transmitting_stations = find(StationTimeToTx == 0);

        % If there are transmitting stations, check for collisions and handle them
        if ~isempty(transmitting_stations)
            % ... (Rest of the collision handling code)
        end

        % ... (Rest of the code)

    end

    % END of INNER LOOP
    % ---------------------------------------------------

    % Save results for the current lambda
    packets(lambda_set) = packets_ok;
    collisions(lambda_set) = packets_col;
    G(lambda_set) = packets_ok / No_slots;
    fprintf("Computing, current lambda = %d/%d\n", lambda_set, length(packet_arrival));
    
    % Calculate S_sim based on the total transmitted packets (G) and No_slots
    S_sim(lambda_set) = G(lambda_set);
    % Calculate S_analyt based on the formula
    S_analyt(lambda_set) = G(lambda_set) * exp(-G(lambda_set));

end

% END OF OUTER LOOP
% ---------------------------------------------------
% ---------------------------------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%
%9. Step: Plot the results%
%%%%%%%%%%%%%%%%%%%%%%%%%%%
LS = 12;
h1 = figure(1);
plot(G, S_analyt, 'b');
hold on
plot(G, S_sim, 'r');
legend('Slotted ALOHA: analyt', 'Slotted ALOHA: simulated', 'Location', 'NorthEast');
xlabel('G');
ylabel('S');
set(gca, 'FontWeight', 'normal', 'FontSize', LS); % 12
grid on;
print('-f1', '-r300', '-dpng', 'Results');
